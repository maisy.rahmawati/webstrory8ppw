from django.test import TestCase,Client
from django .http import HttpRequest
from .views import ajaxpage
from django.urls import resolve

from selenium import webdriver
import unittest
import time

# Create your tests here.
class UnitTest(TestCase):
    #Test untuk cek url dari ajaxpage
    def test_url_address_ajaxpage_is_exist(self):
        response = Client().get('/ajaxpage/')
        self.assertEqual(response.status_code, 200)

    #Test untuk cek alamat url homepage tidak ada
    def test_url_address_ajaxpage_is_not_exist(self):
        response = Client().get('/')
        self.assertNotEqual(response.status_code, 200)

    #Test untuk cek fungsi homepage telah memiliki isi
    def test_ajaxpage_is_written(self):
        self.assertIsNotNone(ajaxpage)

    #Test untuk cek template html telah digunakan pada halaman homepage
    def test_template_html_ajaxpage_is_exist(self):
        response = Client().get('/ajaxpage/')
        self.assertTemplateUsed(response, 'ajaxpage.html')

    #Test untuk cek apakah string accordion telah ada di halaman homepage
    def test_content_html_is_exist(self):
        response = Client().get('/ajaxpage/')
        target = resolve('/ajaxpage/')
        self.assertContains(response,'Search Books')
        self.assertTrue(target.func, ajaxpage)

class FunctionalTest(unittest.TestCase):
    #Melakukan setUp untuk starting web browser, yaitu dengan menggunakan google chrome
    def setUp(self):
        self.browser = webdriver.Chrome()

    #Melakukan tearDown dengan mengeluarkan web browser
    def tearDown(self):
        self.browser.implicitly_wait(3)
        self.browser.quit()

    #Melakukan test functional ke website yang telah di deploy
    def test_functional(self):
        self.browser.get('https://maisy-webstory8ppw.herokuapp.com/ajaxpage/')
        time.sleep(5)
        search_books_box = self.browser.find_element_by_id('books')
        search_books_box.send_keys('harry potter')
        search_books_box.submit()
        time.sleep(10)
        self.assertIn("harry", self.browser.page_source)

if __name__ == '__main__':
   unittest.main(warnings='ignore')
