from django.apps import AppConfig


class AjaxpageConfig(AppConfig):
    name = 'ajaxpage'
