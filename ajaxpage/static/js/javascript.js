$(document).ready(function() {
	$("#ganti_tema1").click(function() {
		$("body").css({"background-color":"#B0C4DE"});
		$(".navbar").css({"background-color":"black"});
		$(".content_page").css({"background-color":"#778899"});
		$(".description p").css({"color":"white"});
		$(".menu_accordion").css({"background-color":"#DCDCDC"});
		$(".tampilan").css({"background-color":"#778899"});
		$(".tampilan p").css({"color":"white"});
		$(".isi_tampilan p").css({"color":"black"});
	});
	$("#ganti_tema2").click(function() {
		$("body").css({"background-color":"#FA8072"});
		$(".navbar").css({"background-color":"#8B4513"});
		$(".content_page").css({"background-color":"#FFDAB9"});
		$(".content_page p").css({"color":"black"});
		$(".menu_accordion").css({"background-color":"#FFEBCD"});
		$(".tampilan").css({"background-color":"#8B4513"});
		$(".tampilan p").css({"color":"black"});
		$(".isi_tampilan p").css({"color":"black"});
	});
	$("#ganti_tema3").click(function() {
		$("body").css({"background-color":"#D2B48C"});
		$(".navbar").css({"background-color":"black"});
		$(".content_page").css({"background-color":"rgba(255,255,255,0.8)"});
		$(".content_page p").css({"color":"black"});
		$(".menu_accordion").css({"background-color":"rgba(255,255,255,0.8)"});
		$(".tampilan").css({"background-color":"rgb(139, 5, 0, 0.7)"});
		$(".tampilan p").css({"color":"white"});
		$(".isi_tampilan p").css({"color":"black"});
	});
	$("#ganti_tema4").click(function() {
		$("body").css({"background-color":"rgb(173,255,48,0.75)"});
		$(".navbar").css({"background-color":"#008000"});
		$(".content_page").css({"background-color":"#BDB76B"});
		$(".content_page p").css({"color":"black"});
		$(".menu_accordion").css({"background-color":"#F0E68C"});
		$(".tampilan").css({"background-color":"rgb(128, 128, 1, 0.9)"});
		$(".tampilan p").css({"color":"white"});
		$(".isi_tampilan p").css({"color":"black"});
	});
	
	
	var key = "https://www.googleapis.com/books/v1/volumes?q=javascript";
		$.getJSON(key, function(data){
				var res = $("#result");
				res.empty();
				for(i = 0; i < data.items.length; i++)
				{	
					res.append(
						"<table>" +
						"<tbody style = 'inline-table'>" +
						"<div class = 'table-set'>" +
						"<tr>" +
						"<td rowspan = '4'>" + '<img class = "aligning card z-depth-5" id = "dynamic" src = "' + data.items[i].volumeInfo.imageLinks.thumbnail + '">' + "<br/>" + "</td>" +
						"<td>" + '<div id = "title">' + data.items[i].volumeInfo.title + '</div>' + "</td>" +
						"</tr>" +
						"<tr>" +
						"<td>" + '<div id = "author">By: ' + data.items[i].volumeInfo.authors + '</div>' + "</td>" +
						"</tr>" +
						"<tr>" +
						"<td>" + '<div id = "catego">Categories: ' + data.items[i].volumeInfo.categories + '</div>' + "</td>" +
						"</tr>" +
						"<tr>" +
						"<td>" + '<a href = ' + data.items[i].volumeInfo.infoLink + '><button id = "imagebutton" class = "btn btn-info">Read More</button></a>' + "<br/>" +"</td>" +
						"</tr>" +
						"</div>" +
						"</tbody>" +
						"</table>"
					);
				}
		});
	
	
	$("#form-books").submit(function(event){
		event.preventDefault();
		var search = $("#books").val();
		var urlKey = "https://www.googleapis.com/books/v1/volumes?q=" + search;
		if(search == ''){
			alert("Please enter title of books in the field first");
		}
		else
		{
			$.ajax({
				method: "GET",
				url: urlKey,
				datatype: JSON,
				success: function(data){
					var res = $("#result");
					res.empty();
					for(i = 0; i < data.items.length; i++)
					{	
						res.append(
							"<table>" +
							"<tbody style = 'inline-table'>" +
							"<div class = 'table-set'>" +
							"<tr>" +
							"<td rowspan = '4'>" + '<img class = "aligning card z-depth-5" id = "dynamic" src = "' + data.items[i].volumeInfo.imageLinks.thumbnail + '">' + "<br/>" + "</td>" +
							"<td>" + '<div id = "title">' + data.items[i].volumeInfo.title + '</div>' + "</td>" +
							"</tr>" +
							"<tr>" +
							"<td>" + '<div id = "author">By: ' + data.items[i].volumeInfo.authors + '</div>' + "</td>" +
							"</tr>" +
							"<tr>" +
							"<td>" + '<div id = "catego">Categories: ' + data.items[i].volumeInfo.categories + '</div>' + "</td>" +
							"</tr>" +
							"<tr>" +
							"<td>" + '<a href = ' + data.items[i].volumeInfo.infoLink + '><button id = "imagebutton" class = "btn btn-info">Read More</button></a>' + "<br/>" +"</td>" +
							"</tr>" +
							"</div>" +
							"</tbody>" +
							"</table>"
						);
					}
				}
			});
		}
	
	});

	return false;

});